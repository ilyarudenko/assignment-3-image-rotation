#include <stdio.h>

#include "bmp.h"
#include "rotate.h"

void print_error (const char *const message) {
    fprintf(stderr, "%s\n", message);
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        print_error("Wrong amount of arguments");

        return 1;
    }

    FILE* in = fopen(argv[1], "rb");

    if (!in) {
        print_error("Bad input file");

        return 2;
    }

    FILE* out = fopen(argv[2],"wb");

    if (!out) {
        print_error("Bad output file");

        fclose(in);
        return 3;
    }

    struct image image = {0};

    enum read_status read_status = from_bmp(in, &image);

    if (read_status != READ_OK) {
        print_error("Errors while reading image");
        printf("%d", read_status);

        free(image.data);
        fclose(in);
        fclose(out);
        return 4;
    }

    struct image rotated_image = rotate(&image);

    enum write_status write_status = to_bmp(out, &rotated_image);

    if (write_status != WRITE_OK) {
        print_error("Errors while writing image");


        free(image.data);
        free(rotated_image.data);
        fclose(in);
        fclose(out);
        return 5;
    }

    free(image.data);
    free(rotated_image.data);
    fclose(in);
    fclose(out);
    return 0;
}
