//
// Created by ilyaa on 25.01.2023.
//

#include "rotate.h"
#include "image.h"

struct image rotate( struct image* image ){

    struct image new_image = {
            .height=image->width,
            .width=image->height,
            .data = malloc(image->width * image->height * sizeof (struct pixel))
    };

    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            new_image.data[image->height * j + image->height - i - 1] = image->data[image->width * i + j];
        }
    }


    return new_image;
}
