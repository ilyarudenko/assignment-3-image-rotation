//
// Created by ilyaa on 25.01.2023.
//

#include "bmp.h"

const uint16_t FILE_TYPE = 19778;
const uint32_t OFF_BITS = 54;
const uint32_t INFO_HEADERS_SIZE = 40;
const uint16_t PLANES = 1;
const uint16_t BIT_COUNT = 24;
const uint32_t X_PIXELS_PER_METER = 2834;
const uint32_t Y_PIXELS_PER_METER = 2834;


static size_t read_header(FILE* file, struct bmp_header* header);
static size_t write_header(FILE* file, struct image* image);

static size_t read_data(FILE* file, struct image* image);
static size_t write_data(FILE* file, struct image* image);

static uint8_t calculate_padding (uint32_t width);

uint8_t calculate_padding(const uint32_t width) {
    return (4 - ((sizeof (struct pixel) * width) % 4 )) % 4;
}

size_t read_header(FILE* file, struct bmp_header* header ) {
    return fread(header, sizeof (struct bmp_header), 1, file) != 1;
}

size_t write_header(FILE* file, struct image* image) {
    struct bmp_header header = {
        .bfType = FILE_TYPE,
        .bfileSize = sizeof (struct bmp_header) + image->height * image->width * sizeof (struct pixel),
        .bfReserved = 0,
        .bOffBits = OFF_BITS,
        .biSize = INFO_HEADERS_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = image->height * image->width * sizeof (struct pixel),
        .biXPelsPerMeter = X_PIXELS_PER_METER,
        .biYPelsPerMeter = Y_PIXELS_PER_METER,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };

    return fwrite(&header, sizeof (struct bmp_header), 1, file) != 1;

}

size_t read_data(FILE* file, struct image* image) {
    uint32_t padding = calculate_padding(image->width);

    printf("%s","width: ");
    printf("%zu\n", image->width);

    printf("%s","height: ");
    printf("%zu\n", image->height);

    for (size_t i = 0; i < image->height; i++) {

        size_t status = fread(image->data + (i * image->width), sizeof(struct pixel), image->width, file);

        if ( status != image->width || fseek(file, padding, SEEK_CUR) != 0) {
            return 1;
        }
    }

    return 0;
}

size_t write_data(FILE* file, struct image* image) {
    size_t padding = calculate_padding(image->width);

    size_t trash[3] = {0, 0, 0};

    for (size_t line =0; line < image->height; line++) {
        size_t status_data = fwrite(image->data + line * image->width, image->width * sizeof (struct pixel),1 , file);
        size_t status_padding = fwrite(trash, padding, 1, file );

        if (status_data == 0 || status_padding == 0) return 1;
    }

    return 0;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if (read_header(in, &header)){
        return READ_INVALID_HEADER;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(img->width* img->height * sizeof (struct pixel));

    if (read_data(in, img)){
        return READ_INVALID_BITS;
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image* img ) {

    if (write_header(out, img)){
        return WRITE_HEADER_ERROR;
    }

    if (write_data(out, img)){
        return WRITE_PIXELS_ERROR;
    }

    return WRITE_OK;
}

