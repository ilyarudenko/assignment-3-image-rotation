//
// Created by ilyaa on 24.01.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include <stdlib.h>

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image* image );

#endif //IMAGE_TRANSFORMER_ROTATE_H




